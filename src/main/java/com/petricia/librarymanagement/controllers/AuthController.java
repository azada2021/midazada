package com.petricia.librarymanagement.controllers;

import com.petricia.librarymanagement.model.BookPick;
import com.petricia.librarymanagement.model.User;
import com.petricia.librarymanagement.service.AuthService;
import com.petricia.librarymanagement.service.BookPickService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "api/auth/")
public class AuthController {
    private final AuthService authService;
    private final BookPickService bookPickService;

    public AuthController(AuthService authService, BookPickService picksRepository) {
        this.authService = authService;
        this.bookPickService = picksRepository;
    }

    private HashMap<?, ?> toJsonErrors(BindingResult bindingResult) {
        HashMap<String, String> map = new HashMap<>();
        bindingResult.getFieldErrors().forEach(f -> map.put(f.getField(), f.getDefaultMessage()));
        return map;
    }

    @PostMapping(value = "signup")
    public ResponseEntity<?> add(@Valid @RequestBody User user, BindingResult bindingResult){
        ResponseEntity<?> entity = null;
        if(bindingResult.hasErrors()){
            entity = new ResponseEntity<>(toJsonErrors(bindingResult).toString(), HttpStatus.BAD_REQUEST);
        }else {

            try {
                authService.insertUser(user);
                entity = new ResponseEntity<>(HttpStatus.CREATED);
            }catch (IllegalArgumentException e){
                entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
            }
        }
        return entity;
    }

    @PostMapping
    public ResponseEntity<?> login(@Valid @RequestBody User user, BindingResult bindingResult){
        ResponseEntity<?> entity = null;
        if(bindingResult.hasErrors()){
            entity = new ResponseEntity<>(toJsonErrors(bindingResult).toString(), HttpStatus.BAD_REQUEST);
        }else {
            try {
                User user1 = authService.login(user);
                entity = new ResponseEntity<>(user1, HttpStatus.CREATED);
            }catch (Exception e){
                entity = new ResponseEntity<>("Incorrect Username/Password.", HttpStatus.BAD_REQUEST);
            }
        }
        return entity;
    }

    @GetMapping(value = "{id}/picks/")
    public ResponseEntity<?> getMyPicks(@PathVariable Long id){
        List<BookPick> myPicks = bookPickService.getMyPicks(id);
        return new ResponseEntity<>(myPicks, HttpStatus.OK);
    }

    @GetMapping(value = "{id}/pick-drops/")
    public ResponseEntity<?> getMyPickAndDrops(@PathVariable Long id){
        List<BookPick> myPicks = bookPickService.getMyPickAndDrops(id);
        return new ResponseEntity<>(myPicks, HttpStatus.OK);
    }
}
