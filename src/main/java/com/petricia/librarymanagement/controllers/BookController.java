package com.petricia.librarymanagement.controllers;

import com.petricia.librarymanagement.model.Book;
import com.petricia.librarymanagement.model.BookPick;
import com.petricia.librarymanagement.repository.BookRepository;
import com.petricia.librarymanagement.service.BookPickService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/books/")
public class BookController {
    private final BookRepository bookRepository;
    private final BookPickService bookPickService;

    public BookController(BookRepository bookRepository, BookPickService bookPickService) {
        this.bookRepository = bookRepository;
        this.bookPickService = bookPickService;
    }

    private HashMap<?, ?> toJsonErrors(BindingResult bindingResult) {
        HashMap<String, String> map = new HashMap<>();
        bindingResult.getFieldErrors().forEach(f -> map.put(f.getField(), f.getDefaultMessage()));
        return map;
    }

    @GetMapping
    public ResponseEntity<?> getAll(@RequestParam(name = "search", required = false) String search){
        List<Book> books = null;
        if(search != null)
            books = bookRepository.searchBook(search.toLowerCase());
        else
            books = bookRepository.findAll();
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> get(@PathVariable Long id){
        Optional<Book> optionalBook = bookRepository.findById(id);
        return new ResponseEntity<>(optionalBook, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> add(@Valid @RequestBody Book book, BindingResult bindingResult){
        ResponseEntity<?> entity = null;
        if(bindingResult.hasErrors())
            entity = new ResponseEntity<>(toJsonErrors(bindingResult), HttpStatus.BAD_REQUEST);
        else{
            book.setAvailable(true);
            bookRepository.saveAndFlush(book);
            entity = new ResponseEntity<>(book, HttpStatus.OK);
        }

        return entity;
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> add(@PathVariable Long id){
        if(bookRepository.existsById(id))
            bookRepository.delete(bookRepository.getOne(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping(value = "{id}/pick/{userId}")
    public ResponseEntity<?> pickBook(@PathVariable Long id, @PathVariable Long userId){
        ResponseEntity<?> entity = null;
        try {
            BookPick pick = bookPickService.pickBook(id, userId);
            entity = new ResponseEntity<>(pick, HttpStatus.OK);
        }catch (IllegalArgumentException e){
            entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return entity;
    }


    @PostMapping(value = "{id}/drop/{userId}")
    public ResponseEntity<?> dropBook(@PathVariable Long id, @PathVariable Long userId){
        ResponseEntity<?> entity = null;
        try {
            BookPick pick = bookPickService.dropBook(id, userId);
            entity = new ResponseEntity<>(pick, HttpStatus.OK);
        }catch (IllegalArgumentException e){
            entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return entity;
    }
}
