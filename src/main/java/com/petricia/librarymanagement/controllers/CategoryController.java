package com.petricia.librarymanagement.controllers;

import com.petricia.librarymanagement.model.Category;
import com.petricia.librarymanagement.repository.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/categories/")
public class CategoryController {
    private final CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    private HashMap<?, ?> toJsonErrors(BindingResult bindingResult) {
        HashMap<String, String> map = new HashMap<>();
        bindingResult.getFieldErrors().forEach(f -> map.put(f.getField(), f.getDefaultMessage()));
        return map;
    }

    @GetMapping
    public ResponseEntity<?> getAll(){
        List<Category> categories = categoryRepository.findAll();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> get(@PathVariable Long id){
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        return new ResponseEntity<>(optionalCategory, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> add(@Valid @RequestBody Category book, BindingResult bindingResult){
        ResponseEntity<?> entity = null;
        if(bindingResult.hasErrors())
            entity = new ResponseEntity<>(toJsonErrors(bindingResult), HttpStatus.BAD_REQUEST);
        else{
            categoryRepository.saveAndFlush(book);
            entity = new ResponseEntity<>(book, HttpStatus.OK);
        }

        return entity;
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> add(@PathVariable Long id){
        if(categoryRepository.existsById(id))
            categoryRepository.delete(categoryRepository.getOne(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
