package com.petricia.librarymanagement.controllers;

import com.petricia.librarymanagement.model.Book;
import com.petricia.librarymanagement.model.Category;
import com.petricia.librarymanagement.model.Library;
import com.petricia.librarymanagement.repository.BookRepository;
import com.petricia.librarymanagement.repository.CategoryRepository;
import com.petricia.librarymanagement.repository.LibraryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/libraries/")
public class LibraryController {
    private final LibraryRepository libraryRepository;
    private final BookRepository bookRepository;
    private final CategoryRepository categoryRepository;

    public LibraryController(LibraryRepository libraryRepository, BookRepository bookRepository, CategoryRepository categoryRepository) {
        this.libraryRepository = libraryRepository;
        this.bookRepository = bookRepository;
        this.categoryRepository = categoryRepository;
    }

    private HashMap<?, ?> toJsonErrors(BindingResult bindingResult) {
        HashMap<String, String> map = new HashMap<>();
        bindingResult.getFieldErrors().forEach(f -> map.put(f.getField(), f.getDefaultMessage()));
        return map;
    }

    @GetMapping
    public ResponseEntity<?> getAll(){
        List<Library> libraries = libraryRepository.findAll();
        return new ResponseEntity<>(libraries, HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> get(@PathVariable Long id){
        Optional<Library> library = libraryRepository.findById(id);
        return new ResponseEntity<>(library, HttpStatus.OK);
    }

    @GetMapping(value = "{id}/categories")
    public ResponseEntity<?> getCategories(@PathVariable Long id){
        List<Category> categories = categoryRepository.findAllByLibraryId(id);
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping(value = "{id}/books")
    public ResponseEntity<?> getBooks(@PathVariable Long id){
        List<Book> books = bookRepository.findAllByCategoryLibraryId(id);
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> add(@Valid @RequestBody  Library library, BindingResult bindingResult){
        ResponseEntity<?> entity = null;
        if(bindingResult.hasErrors())
            entity = new ResponseEntity<>(toJsonErrors(bindingResult), HttpStatus.BAD_REQUEST);
        else{
            libraryRepository.saveAndFlush(library);
            entity = new ResponseEntity<>(library, HttpStatus.OK);
        }

        return entity;
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> add(@PathVariable Long id){
        if(libraryRepository.existsById(id))
            libraryRepository.delete(libraryRepository.getOne(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
