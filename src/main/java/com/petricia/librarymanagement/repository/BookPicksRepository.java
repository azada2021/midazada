package com.petricia.librarymanagement.repository;

import com.petricia.librarymanagement.model.BookPick;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookPicksRepository extends JpaRepository<BookPick, Long> {
    List<BookPick> findAllByUserIdAndBookIdAndType(Long userId, Long bookId, String type);
    List<BookPick> findAllByUserId(Long userId);
    List<BookPick> findAllByUserIdAndType(Long userId, String type);
}
