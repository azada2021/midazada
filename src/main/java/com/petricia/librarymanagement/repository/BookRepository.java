package com.petricia.librarymanagement.repository;

import com.petricia.librarymanagement.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByCategoryLibraryId(Long id);

    @Query("SELECT b FROM Book b WHERE lower(b.name) LIKE %?1% OR lower(b.author) LIKE %?1% OR lower(b.category.name) LIKE %?1%")
    List<Book> searchBook(String name);
}
