package com.petricia.librarymanagement.repository;

import com.petricia.librarymanagement.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findAllByLibraryId(Long id);
}
