package com.petricia.librarymanagement.repository;

import com.petricia.librarymanagement.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LibraryRepository extends JpaRepository<Library, Long> {
}
