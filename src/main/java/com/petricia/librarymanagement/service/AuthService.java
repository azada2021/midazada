package com.petricia.librarymanagement.service;

import com.petricia.librarymanagement.model.User;
import com.petricia.librarymanagement.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService {
    private final UserRepository userRepository;

    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void insertUser(User user){
        Optional<User> optionalUser = userRepository.findByEmail(user.getEmail());
        if(optionalUser.isPresent())
            throw new IllegalArgumentException("User is already available with this email");
        user.setPassword(user.getPassword());
        userRepository.saveAndFlush(user);
    }

    public User login(User user){
        Optional<User> optionalUser = userRepository.findByEmail(user.getEmail());
        if(optionalUser.isPresent() && optionalUser.get().getPassword().equals(user.getPassword()))
            return optionalUser.get();
        else
            throw new IllegalArgumentException("Bad Credentials");
    }
}
