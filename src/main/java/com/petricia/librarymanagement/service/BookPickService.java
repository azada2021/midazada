package com.petricia.librarymanagement.service;

import com.petricia.librarymanagement.model.Book;
import com.petricia.librarymanagement.model.BookPick;
import com.petricia.librarymanagement.repository.BookPicksRepository;
import com.petricia.librarymanagement.repository.BookRepository;
import com.petricia.librarymanagement.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.petricia.librarymanagement.model.BookPick.TYPE.DROP;
import static com.petricia.librarymanagement.model.BookPick.TYPE.PICK;

@Service
public class BookPickService {
    private final BookPicksRepository picksRepository;
    private final BookRepository bookRepository;
    private final UserRepository userRepository;

    public BookPickService(BookPicksRepository picksRepository, BookRepository bookRepository, UserRepository userRepository) {
        this.picksRepository = picksRepository;
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
    }

    public List<BookPick> getMyPicks(Long id){
        return picksRepository.findAllByUserIdAndType(id, PICK);
    }

    public List<BookPick> getMyPickAndDrops(Long id){
        return picksRepository.findAllByUserId(id);
    }

    public BookPick dropBook(Long bookId, Long userId){
        Optional<Book> optionalBook = bookRepository.findById(bookId);
        if(optionalBook.isEmpty())
            throw new IllegalArgumentException(String.format("Book with id %s not found.", bookId));

        Book book = optionalBook.get();

        if(book.getCurrentPick() == null || !book.getCurrentPick().getUser().getId().equals(userId))
            throw new IllegalArgumentException("You haven't picked this book before. You can't return it");

        BookPick pick =new BookPick();
        pick.setBook(book);
        pick.setType(DROP);
        pick.setUser(userRepository.findById(userId).get());

        picksRepository.saveAndFlush(pick);
//        inverting the availability if everything is normal.
        book.setAvailable(!book.isAvailable());
        book.setCurrentPick(null);
        bookRepository.flush();

        return pick;
    }

    public BookPick pickBook(Long bookId, Long userId){
        Optional<Book> optionalBook = bookRepository.findById(bookId);
        if(optionalBook.isEmpty())
            throw new IllegalArgumentException(String.format("Book with id %s not found.", bookId));

        Book book = optionalBook.get();

        if(book.getCurrentPick() != null && book.getCurrentPick().getUser().getId().equals(userId))
            throw new IllegalArgumentException("Please return this book back. You have already picked it.");

        if(!book.isAvailable())
            throw new IllegalArgumentException("This book isn't available.");


        BookPick pick =new BookPick();
        pick.setBook(book);
        pick.setType(PICK);

        pick.setUser(userRepository.findById(userId).get());

        picksRepository.saveAndFlush(pick);

        book.setCurrentPick(pick);
//        inverting the availability if everything is normal.
        book.setAvailable(!book.isAvailable());
        bookRepository.flush();

        return pick;
    }
}
